import random
import tkinter as tk


####################################################################################################
class Colony():
    def __init__(self, number = 1000) -> None:
        self.number:int = number
        self.dots = []
        self.fill()
        self.fitnesSum = 0
        self.generation = 1

    def fill(self):
        for i in range(self.number):
            self.dots.append(Dot(i))
            
    def atleastOneDotIsAlive(self) -> bool:
        for dot in self.dots:
            if not dot.dead and not dot.reachedGoal and not dot.brain.empty:
                return True
        return False
    
    def calculateFitness(self):
         for dot in self.dots:
             dot.calculateFitness()           
                     
    def naturalSelection(self):
        dots = []
        dots.append(self.findTheBest())  
        self.calculateFitnesSum() 
        for i in range(self.number - 1):
            parent = self.selectParent()
            dots.append(parent.gimmeBaby())
        self.dots = dots   
        self.generation += 1 

        
    def findTheBest(self):
        best = Dot("test", "gold")
        for dot in self.dots:
            if dot.fitnes > best.fitnes:
                best = dot
        best.refresh()        
        best.color = "red"
        return best        
    
    def calculateFitnesSum(self):   
        for dot in self.dots:
            self.fitnesSum += dot.fitnes       
            
    def selectParent(self):   
        randomNumber = random.randint(0, round(self.fitnesSum))     
        runningSum = 0
        for dot in self.dots:
            runningSum += dot.fitnes
            if(runningSum > randomNumber):
                 return dot      
        return self.dots[0]
    
    def mutateDemBabbies(self):
        for dot in self.dots:
            dot.brain.mutate()  
####################################################################################################
class Dot():
    def __init__(self, name, color = "black") -> None:
        self.coord = Coord(100, 700)
        self.color = color
        self.brain = Brain(250)
        self.dead = False
        self.fitnes = 0
        self.reachedGoal = False
        self.name = name
        
    def move(self):
        if self.dead or self.reachedGoal:
            return
        if len(self.brain.directions) > self.brain.step:
            acc = self.brain.directions[self.brain.step]
            self.brain.step += 1
            self.coord.x += acc.x
            self.coord.y += acc.y
            self.check()
        else:
            self.brain.empty = True      
        
    def check(self):
        if self.coord.x <= 10 or self.coord.x >= 790 or self.coord.y <= 10 or self.coord.y >= 790:
            self.dead = True  
        elif self.coord.x >= 350 and self.coord.x <= 650 and self.coord.y >= 500 and self.coord.y <= 520:
            self.dead = True   
        elif self.coord.x >= 150 and self.coord.x <= 450 and self.coord.y >= 250 and self.coord.y <= 270:
            self.dead = True   
        elif self.coord.x > 690 and self.coord.x < 710 and self.coord.y > 70 and self.coord.y < 90:
            self.reachedGoal = True
                    
    def calculateFitness(self):
        first = (self.coord.x - 700) ** 2
        second = (self.coord.y - 80) ** 2
        distance = (first + second) ** (1/2)
        odmena = 0
        if self.reachedGoal:
            odmena = 1000
        if self.dead:
            odmena = (-1000)     
        self.fitnes = odmena + 5000 - (distance*3) - (self.brain.step*3)
        
    def gimmeBaby(self):
        baby = Dot(self.name)
        baby.brain = self.brain.clone()
        return baby
    
    def refresh(self):
        self.reachedGoal = False
        self.dead = False
        self.fitnes = 0
        self.coord = Coord(100, 700)
        self.color = "black"
        oldBrain = self.brain
        self.brain = Brain(oldBrain.size)
        self.brain.directions = oldBrain.directions
        self.brain.step = 0 
####################################################################################################
class Coord():
    def __init__(self, x, y) -> None:
        self.x = x
        self.y = y
####################################################################################################
class Brain():
    def __init__(self, size) -> None:
        self.size = size
        self.directions = []
        self.step = 0
        self.fill()
        self.randomize()
        self.empty = False
        
    def fill(self):
        for i in range(self.size):
            self.directions.append(Coord(0,0))    
        
    def randomize(self):
        for i in range(self.size):
            x = random.randint(0,40) - 20
            y = random.randint(0,40) - 20
            self.directions[i] = Coord(x,y)
            
    def clone(self):
        clone = Brain(self.size)
        clone.directions = []
        clone.directions.extend(self.directions)
        return clone   
    
    def mutate(self):
        mutationRate = 0.01
        for i in range(len(self.directions) - 1):
            if i == 0:
                continue
            rand = random.random()
            if rand < mutationRate:
                x = random.randint(0,40) - 20
                y = random.randint(0,40) - 20
                self.directions[i] = Coord(x,y)                     
####################################################################################################
class App:
    def __init__(self, colony:Colony) -> None:
        self.colony = colony
        self.window = tk.Tk()
        self.window.title("AI game")
        self.canvas = tk.Canvas(self.window, bg="white",height=800, width=800)
        self.canvas.pack()
        self.window.after(100, lambda: self.update())
        self.window.mainloop()
        
    def clean(self):
        self.canvas.delete("all")    
        
    def drawDot(self, x, y, r, color):
        self.canvas.create_oval(x - r, y - r, x + r, y + r, fill=color, outline="gold")
        
    def printLabel(self):
        Label_middle = tk.Label(self.window, text = "generation " + str(self.colony.generation), background="white", font=("Arial", 15), fg="red")    
        Label_middle.place(relx = 0.8,rely = 0.8,anchor ='w')
        
        
    def update(self):
        if self.colony.atleastOneDotIsAlive():
            self.clean()
            self.printLabel()
            self.drawDot(700, 80, 10, "gold")
            self.canvas.create_rectangle(350, 500, 650, 520, fill="black")
            self.canvas.create_rectangle(150, 250, 450, 270, fill="black")
            for dot in self.colony.dots:
                dot.move()
                self.drawDot(dot.coord.x, dot.coord.y, 4, dot.color)
            self.window.after(20, lambda: self.update())  
        else:
            self.colony.calculateFitness()
            self.colony.naturalSelection()
            self.colony.mutateDemBabbies()
            self.window.after(1000,lambda: self.update())  
####################################################################################################
def main():
    colony = Colony()
    app = App(colony)
####################################################################################################
if __name__ == '__main__':
    main()
